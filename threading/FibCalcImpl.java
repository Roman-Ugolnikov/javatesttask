
/**
 * A fibonacci calculator.
 */
public class FibCalcImpl implements FibCalc{

	private volatile long previosPrevious = 1;

	private volatile long previos = 0;

	private volatile int currrentN = 1;

	/**
	 * Reset in order to be able to calculate fibonachi again
	 */
	private void reset(){
		previosPrevious = 1;
		previos = 0;
		currrentN = 1;
	}

	/**
	 * Calculates the given fibonacci number.
	 * Examples:
	 * fib(1) = 1    <br>
	 * fib(2) = 1    <br>
	 * fib(3) = 2    <br>
	 * fib(4) = 3    <br>
	 * fib(5) = 5    <br>
	 */
	@Override
	public synchronized long fib(int n) {
		if(n < 1 ){
			throw new IllegalArgumentException("We calculate fibonacci only for positive numbers ");
		}

		if(currrentN == n){
			return previos + previosPrevious;
		} else {
			long currentFib = fib(currrentN);
			previosPrevious = previos;
			previos = currentFib;
			currrentN++;
			long fib = fib(n);
			reset();
			return fib;
		}
	}

}
