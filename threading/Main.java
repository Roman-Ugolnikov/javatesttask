
public class Main {

    public static void main(String[] args) throws InterruptedException {
        if (args.length < 3) {
            throw new IllegalArgumentException("we expect to get 3 arguments: " +
                    "<n> <calculationCount> <threadPoolSize>");
        }
        //not doing try. Assuming we get right int format ( > 0)
        int nFiboncci = Integer.parseInt(args[0]);
        int calculationCount = Integer.parseInt(args[1]);
        int threadPoolSize = Integer.parseInt(args[2]);

        FibCalc fibCalc = new FibCalcImpl();
        PerformanceTester performanceTester = new PerformanceTesterImpl();
        performanceTester.runPerformanceTest(() -> fibCalc.fib(nFiboncci), calculationCount, threadPoolSize);
    }
}
