
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PerformanceTesterImpl implements PerformanceTester {

    private static volatile long min = Long.MAX_VALUE;

    private static volatile long max = Long.MIN_VALUE;

    private ExecutorService service;

    @Override
    public PerformanceTestResult runPerformanceTest(Runnable task,
                                                    int executionCount,
                                                    int threadPoolSize) throws InterruptedException {
        long start = System.currentTimeMillis();

        service = Executors.newFixedThreadPool(executionCount);

        // case 1: more threads then executions
        // case 2: equals
        if ((executionCount == threadPoolSize)
                || (executionCount < threadPoolSize)) {
            for (int i = 0; i < executionCount; i++) {
                service.execute(getDecoratedMultipleRunnable(getDecoratedRunnable(task), 1));
            }
        } else {
            // case 3: counts more then threads
            int executionPerThread = executionCount / threadPoolSize;
            int lastExecutionPerThread = executionPerThread + (executionCount % threadPoolSize);
            for (int threadIndex = 0; threadIndex < threadPoolSize; threadIndex++) {
                int currentExecutionPerThread;
                if (threadIndex + 1 == threadPoolSize) {
                    //last thread which will execut div remnants
                    currentExecutionPerThread = lastExecutionPerThread;
                } else {
                    //eny except last
                    currentExecutionPerThread = executionPerThread;
                }
                service.execute(getDecoratedMultipleRunnable(getDecoratedRunnable(task), currentExecutionPerThread));
            }
        }


        service.shutdown();
        while(!service.isTerminated()){
            //when every thread finished!!!!!!
        }

        long totalTime = System.currentTimeMillis() - start;
        PerformanceTestResult result = new PerformanceTestResult(totalTime, min, max);
        System.out.println("min time " + result.getMinTime());
        System.out.println("max time " + result.getMaxTime());
        System.out.println("total time " + result.getTotalTime());
        return result;
    }

    /**
     * Task that is decorated with multiplying provided task.
     *
     * @param task task to be executed
     * @return decorated task
     */
    private Runnable getDecoratedMultipleRunnable(Runnable task, int times) {
        return () -> {
            long start = System.currentTimeMillis();
            for (int i = 0; i < times; i++) {
                task.run();
            }
            long total = System.currentTimeMillis() - start;
            updateMetaInfo(total);
        };
    }

    /**
     * Task that is decorated with measuring of execution time.
     *
     * @param task task to be executed
     * @return decorated task
     */
    private Runnable getDecoratedRunnable(Runnable task) {
        return () -> {
            long start = System.currentTimeMillis();
            task.run();
            long total = System.currentTimeMillis() - start;
            updateMetaInfo(total);
        };
    }

    /**
     * Update min and max values in thread safe manner.
     *
     * @param timeOfExecution
     */
    public static synchronized void updateMetaInfo(long timeOfExecution) {
        if (min > timeOfExecution) {
            min = timeOfExecution;
        }

        if (max < timeOfExecution) {
            max = timeOfExecution;
        }
    }
}
