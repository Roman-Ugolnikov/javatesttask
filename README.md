# 1: Refactor
### Time spent:
About 3-4 hours: 2 to understand application and business model,  1,5 - 2 to do refactoring.

# 2: CacheMap
I understood that I should not reinvent the wheel and it is allowed to base my implementation
on java.util.Map. The only thing that should be design: caching.
### Time spent:
1h 40m

# 3: Multithreading
I understood that I should calculate fibonacci only for positive numbers grater then 0.
### Time spent:
4-5 hours

# 4: Web application
Web UI is not nice. I focused on BE part.
### Time spent:
7-9 hours