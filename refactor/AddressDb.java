
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Persistance level class, that gives us oportunity to do DB level operation.
 */
public class AddressDb {

	//to be perfect, this properties should be set through IoC container.
	private String JDBC_CONNECTION_URL; // = "jdbc:oracle:thin:@prod";
	private String JDBC_DRIVER_NAME;    // = "oracle.jdbc.ThinDriver";
	private String JDBC_USERNAME;       // = "admin";
	private String JDBC_PASSWORD;       // = "beefhead";


	{
		try {
			Class.forName(JDBC_DRIVER_NAME);
		} catch (ClassNotFoundException e) {
			logger.ERROR("can not get JDBC driver", e);
		}
	}


	public AddressDb(String JDBC_CONNECTION_URL, String JDBC_DRIVER_NAME, String JDBC_USERNAME, String JDBC_PASSWORD) {
		this.JDBC_CONNECTION_URL = JDBC_CONNECTION_URL;
		this.JDBC_DRIVER_NAME = JDBC_DRIVER_NAME;
		this.JDBC_USERNAME = JDBC_USERNAME;
		this.JDBC_PASSWORD = JDBC_PASSWORD;
	}

	public void addEntry(AddressEntry addressEntry) {
		String sql = "insert into AddressEntry values (?, ?, ?)";
		try (
				Connection connection = DriverManager.getConnection(JDBC_CONNECTION_URL, JDBC_USERNAME, JDBC_PASSWORD);
				PreparedStatement statement = createStatement(connection, sql);
		) {
			statement.setLong(1, System.currentTimeMillis());
			statement.setString(2, addressEntry.getName());
			statement.setString(3, addressEntry.getPhoneNumber());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}


	/**
	 * Find address record by phone number prefix
	 *
	 * @param phonePrefix the beginning of the number.
	 */
	public List<AddressEntry> findByPhonePrefix(String phonePrefix) {
		String sql = "select * from AddressEntry where phoneNumber like %?";
		List<AddressEntry> entries = getAddressEntries(phonePrefix, sql);
		return entries != null ? entries : Collections.emptyList();
	}


	/**
	 * Find address record by person name
	 *
	 * @param personName exact match of the person name.
	 */
	public List<AddressEntry> findByPersonName(String personName) {
		String sql = "select * from AddressEntry where name like ?";
		List<AddressEntry> entries = getAddressEntries(personName, sql);
		return entries != null ? entries : Collections.emptyList();
	}


	/**
	 * Get all possible records from address book
	 */
	public List<AddressEntry> getAll() {
		String sql = "select * from AddressEntry";
		try (
				Connection connection = DriverManager.getConnection(JDBC_CONNECTION_URL, JDBC_USERNAME, JDBC_PASSWORD);
				PreparedStatement statement = createStatement(connection, sql);
				ResultSet result = statement.executeQuery();
		) {
			List<AddressEntry> entries = new LinkedList<AddressEntry>();
			while (result.next()) {
				String name = result.getString("name");
				String phoneNumber = result.getString("phoneNumber");
				entries.add(new AddressEntry(name, phoneNumber));
			}
			return entries;
		} catch (SQLException e) {
			logger.ERROR("Unable to fetch all entities", e);
		}
		return Collections.emptyList();
	}


	/**
	 * Get count of records in address book
	 */
	public int getCount() {
		String sql = "select count(*) from AddressEntry";
		try (
				Connection connection = DriverManager.getConnection(JDBC_CONNECTION_URL, JDBC_USERNAME, JDBC_PASSWORD);
				PreparedStatement statement = createStatement(connection, sql);
				ResultSet result = statement.executeQuery();
		) {

			if (result.next()) {
				return result.getInt(1);
			}
		} catch (SQLException e) {
			logger.ERROR("Unable to fetch all entities", e);
		}
		return -1;
	}


	private PreparedStatement createStatement(Connection connection, String sql, String... args) throws SQLException {
		PreparedStatement statement = connection.prepareStatement(sql);
		for (int i = 1; i <= args.length; i++) {
			statement.setString(i, args[i]);
		}
		return statement;

	}


	private List<AddressEntry> getAddressEntries(String parameter, String sql) {
		try (
				Connection connection = DriverManager.getConnection(JDBC_CONNECTION_URL, JDBC_USERNAME, JDBC_PASSWORD);
				PreparedStatement statement = createStatement(connection, sql, parameter);
				ResultSet result = statement.executeQuery();
		) {
			List<AddressEntry> entries = new LinkedList<AddressEntry>();
			while (result.next()) {
				String name = result.getString("name");
				String phoneNumber = result.getString("phoneNumber");
				entries.add(new AddressEntry(name, phoneNumber));
			}
			return entries;
		} catch (SQLException e) {
			logger.ERROR("Unable to fetch all entities", e);
		}
		return null;
	}

}