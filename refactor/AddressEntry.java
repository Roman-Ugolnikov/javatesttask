
import java.time.Instant;
import java.util.Date;

/**
 * Entity class that represent adress.
 */
public class AddressEntry {
	private String name;
	private String phoneNumber;
	private Date creationDate;

	public AddressEntry(String name, String phoneNumber) {
		this.name = name;
		this.phoneNumber = phoneNumber;
		this.creationDate = Date.from(Instant.now());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
}