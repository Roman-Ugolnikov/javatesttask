
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Service level class
 */
public class AddressBook {

	private static final String SWEDEN_DIALING_CODE = "+46";
	private static final String SWEDEN_PHONE_START = "070";
	private static final int NUMBER_MINIMAL_LENGTH = 3;

	private AddressDb addressService;

	public AddressBook(AddressDb addressService) {
		this.addressService = addressService;
	}

	/**
	 * Check whether given person has phone number
	 * @param name exact match of person name
	 */
	public boolean hasMobile(String name) {
		String mobile = getMobile(name);
		if ((mobile != null)
				&& (mobile.length() > NUMBER_MINIMAL_LENGTH)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get the size of the address book.
	 */
	public int getSize() {
		return addressService.getCount();
	}

	/**
	 * Get the given user's mobile phone number,
	 * or null if he doesn't have one. In case we have couple of users with the same name but
	 * with different number method returns first match.
	 */
	public String getMobile(String name) {
		AddressEntry addressEntry = null;
		List<AddressEntry> byPersonName = addressService.findByPersonName(name);
		if(!byPersonName.isEmpty()){
			addressEntry = byPersonName.get(0);
		}
		return addressEntry != null ? addressEntry.getPhoneNumber() : null;
	}

	/**
	 * Returns all names in the book. Truncates to the given length.
	 */
	public List getNames(int maxLength) {

		return addressService.getAll().stream()
				.map(AddressEntry::getName)
				.filter(Objects::nonNull)
				.map(name -> name.length() > maxLength ? name.substring(0, maxLength) : name)
				.collect(Collectors.toList());
	}

	/**
	 * Returns all people who have mobile phone numbers with specified prefix.
	 *
	 * @param prefix prefix to be checked. Note: it should be exact match - leading '+' should be
	 *               explicitly specified in prefix.
	 */
	public List getListByPhonePrefix(String prefix) {
		return addressService.findByPhonePrefix(prefix).stream()
				.filter(addressEntry -> addressEntry.getPhoneNumber() != null)
				.filter(addressEntry -> addressEntry.getPhoneNumber().startsWith(prefix))
				.collect(Collectors.toList());
	}


	/**
	 * Returns all people who have mobile phone numbers in Sweden.
	 */
	public List getListForSweden() {
		List<AddressEntry> addressEntries = new ArrayList<>();
		addressEntries.addAll(getListByPhonePrefix(SWEDEN_PHONE_START));
		addressEntries.addAll(getListByPhonePrefix(SWEDEN_DIALING_CODE + SWEDEN_PHONE_START));
		return addressEntries;

	}

	/**
	 * Returns all people who have mobile phone numbers.
	 */
	public List getList() {
		return addressService.getAll().stream()
				.filter(addressEntry -> addressEntry.getPhoneNumber() != null)
				.collect(Collectors.toList());
	}
}