CREATE TABLE author
(
  id   BIGINT AUTO_INCREMENT
    PRIMARY KEY,
  name VARCHAR(255) NULL
);


CREATE TABLE book
(
  id        BIGINT AUTO_INCREMENT
    PRIMARY KEY,
  title     VARCHAR(255) NULL,
  language  VARCHAR(255) NULL,
  author_id BIGINT       NULL,
  CONSTRAINT FKklnrv3weler2dfky958
  FOREIGN KEY (author_id) REFERENCES author (id)
);
CREATE INDEX FKklnrv3wel12ftkweewlky958
  ON book (author_id);


