package com.example.bookstore.presentation;

import com.example.bookstore.domain.Book;
import com.example.bookstore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;


@Controller
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) {
        bookService.createAuthor("some name");
        return "index";
    }


    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public String books(Map<String, Object> model, @PageableDefault(sort="title", size = 20) Pageable pageabl) {
        model.put("books", bookService.getBooks(pageabl));
        return "books";
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    public String booksPost(@ModelAttribute Book book) {
        if (book != null)
            bookService.updateBook(book);
        return "redirect:/books";
    }


    @RequestMapping("/books/{id}")
    public String books(Map<String, Object> model, @PathVariable("id") Long id) {
        Book book = bookService.getBook(id);
        model.put("authors", bookService.getAllAuthors());
        if (book != null) {
            model.put("book", book);
        } else {
            model.put("book", new Book());
        }
        return "book";
    }


    @RequestMapping("/books/{id}/delete")
    public String authorRemove(Map<String, Object> model, @PathVariable("id") Long id) {
        bookService.removeBook(id);
        return "redirect:/books";
    }
}
