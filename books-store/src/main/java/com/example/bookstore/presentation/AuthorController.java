package com.example.bookstore.presentation;

import com.example.bookstore.domain.Author;
import com.example.bookstore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;


@Controller
public class AuthorController {


    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/authors", method = RequestMethod.GET)
    public String books(Map<String, Object> model, @PageableDefault(sort="name", size = 20) Pageable pageabl) {
        model.put("authors", bookService.getAuthors(pageabl));
        return "authors";
    }

    @RequestMapping(value = "/authors", method = RequestMethod.POST)
    public String booksPost(@ModelAttribute Author author) {
        if (author != null)
            bookService.updateAuthor(author);
        return "redirect:/authors";
    }


    @RequestMapping("/authors/{id}")
    public String books(Map<String, Object> model, @PathVariable("id") Long id) {
        Author author = bookService.getAuthor(id);
        if (author != null) {
            model.put("author", author);
        } else {
            model.put("author", new Author());
        }
        return "author";
    }

    @RequestMapping("/authors/{id}/delete")
    public String authorRemove(Map<String, Object> model, @PathVariable("id") Long id) {
        bookService.removeAuthor(id);
        return "redirect:/authors";
    }
}
