package com.example.bookstore.service;

import com.example.bookstore.domain.Author;
import com.example.bookstore.domain.Book;
import com.example.bookstore.persistence.AuthorRepository;
import com.example.bookstore.persistence.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BookService {
    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    @Autowired
    public BookService(BookRepository bookRepository,
                       AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }

    public Author createAuthor(String name) {
        return authorRepository.save(new Author(name));
    }

    public Author updateAuthor(Author author){
        return authorRepository.save(author);
    }

    public Author getAuthor(Long id){
        return authorRepository.findOne(id);
    }

    public Page<Author> getAuthors(Pageable pageable){
        return authorRepository.findAll(pageable);
    }

    public Page<Book> getBooks(Pageable pageable){
        return bookRepository.findAll(pageable);
    }

    public Book getBook(Long id){
        return bookRepository.findOne(id);
    }

    public Book updateBook(Book book){
        return bookRepository.save(book);
    }

    public List<Author> getAllAuthors(){
        return authorRepository.getAllOrderByName();
    }

    public void removeAuthor(Long id){
        authorRepository.delete(id);
    }

    public void removeBook(Long id){
        bookRepository.delete(id);
    }
}
