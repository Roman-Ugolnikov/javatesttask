package com.example.bookstore.service;

import com.example.bookstore.domain.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Locale;

@Service
public class AuthorFormatter implements Formatter<Author> {

    private BookService bookService;

    @Autowired
    public AuthorFormatter(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public Author parse(String s, Locale locale) throws ParseException {
        return bookService.getAuthor(Long.valueOf(s));
    }

    @Override
    public String print(Author author, Locale locale) {
        return author.getName();
    }
}
