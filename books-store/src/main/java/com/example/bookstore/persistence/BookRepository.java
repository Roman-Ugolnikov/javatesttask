package com.example.bookstore.persistence;

import com.example.bookstore.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    @Query("select  book from Book book join fetch book.author")
    List<Book> getAllEgar();

}
