package com.example.bookstore.persistence;

import com.example.bookstore.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AuthorRepository extends JpaRepository<Author, Long> {

    @Query("select author from Author author order by author.name")
    List<Author> getAllOrderByName();
}
