package com.bics.roamt.presentation.v1.controller;


import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

public class CacheMapImpl<K, V> implements CacheMap<K, V> {

    Logger logger = Logger.getLogger(CacheMapImpl.class.toString());

    /**
     * Default time to live
     */
    private long timeToLive = 10000;

    private final Map<K, TimedValue<V>> kvMap = new HashMap<>();

    @Override
    public void setTimeToLive(long timeToLive) {
        this.timeToLive = timeToLive;
    }

    @Override
    public long getTimeToLive() {
        return timeToLive;
    }

    @Override
    public V put(K key, V value) {
        clearExpired();
        V previous = null;
        if (containsKey(key)) {
            previous = kvMap.get(key).getValue();
        }
        kvMap.put(key, new TimedValue<V>(value));
        return previous;
    }

    @Override
    public void clearExpired() {
        logger.info("Start cleaning");
        for (K key : kvMap.keySet()) {
            if (kvMap.get(key).isExpired(timeToLive)) {
                kvMap.remove(key);
            }
        }
    }

    @Override
    public void clear() {
        kvMap.clear();

    }

    @Override
    public boolean containsKey(Object key) {
        clearExpired();
        return kvMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        clearExpired();
        long count = kvMap.values().stream()
                .filter(vTimedValue -> Objects.equals(vTimedValue.getValue(), value))
                .count();
        return count > 0;
    }

    @Override
    public V get(Object key) {
        clearExpired();
        TimedValue<V> vTimedValue = kvMap.get(key);
        return (vTimedValue == null) ? null : vTimedValue.getValue();
    }

    @Override
    public boolean isEmpty() {
        clearExpired();
        return kvMap.isEmpty();
    }

    @Override
    public V remove(Object key) {
        clearExpired();
        TimedValue<V> removed = kvMap.remove(key);
        return removed != null ? removed.getValue() : null;
    }

    @Override
    public int size() {
        clearExpired();
        return kvMap.size();
    }

    private class TimedValue<V> {
        V value;
        long created;

        public TimedValue(V value) {
            this.value = value;
            this.created = Clock.getTime();
        }

        private boolean isExpired(long timeToLIve) {
            return Clock.getTime() - created > timeToLIve ? true : false;
        }

        public V getValue() {
            return value;
        }
    }
}
